##
# Copyright (c) 2018-2022, NXOS Development Team
# SPDX-License-Identifier: Apache-2.0
# 
# Contains: Makefile for Source code
# 
# Change Logs:
# Date           Author            Notes
# 2022-1-31      JasonHu           Init
##

MAKE	:=make
CP		:=cp

SDK_DIR		:= ../sdk
SDK_INC_DIR	:= $(SDK_DIR)/include
SDK_LIB_DIR	:= $(SDK_DIR)/lib

LIBNXOS_LIB		:= ./build/libnxos.a
LIBNXOS_INC_DIR	:= ./src/include

ifeq ($(O),)
O = $(CURDIR)/build
endif

#
# Cmds
#
.PHONY: all clean

all:
	@$(MAKE) -s -C src O=$(O)

clean:
	@$(MAKE) -s -C src clean O=$(O)

install:
	@$(CP) $(LIBNXOS_LIB) $(SDK_LIB_DIR)
	@$(CP) -r $(LIBNXOS_INC_DIR)/* $(SDK_INC_DIR)