/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: Init OS 
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2021-11-15     JasonHu           Init
 */

#include <nxos/initcall.h>

/**
 * init call from link script
 */
NX_IMPORT NX_InitCallHandler __NX_InitCallStart[];
NX_IMPORT NX_InitCallHandler __NX_InitCallEnd[];
NX_IMPORT NX_InitCallHandler __NX_ExitCallStart[];
NX_IMPORT NX_InitCallHandler __NX_ExitCallEnd[];

void NX_CallInvoke(NX_InitCallHandler start[], NX_InitCallHandler end[])
{
	NX_InitCallHandler *func =  &(*start);
	for (;func < &(*end); func++)
    {
		(*func)();
    }
}

void NX_InitCallInvoke(void)
{
    NX_CallInvoke(__NX_InitCallStart, __NX_InitCallEnd);
}

void NX_ExitCallInvoke(void)
{
    NX_CallInvoke(__NX_ExitCallStart, __NX_ExitCallEnd);
}
