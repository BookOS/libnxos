/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: memory management api
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-4-20      JasonHu           Init
 */

#include <nxos/mman.h>
#include <nxos/syscall.h>

void * NX_MemMap(void * addr, NX_Size length, NX_U32 prot)
{
    void * mappedAddr;
    NX_Error err;
    
    mappedAddr = (void *)NX_Syscall4(NX_API_MemMap, addr, length, prot, &err);
    NX_ErrorSet(err);

    return mappedAddr;
}

void * NX_MemMap2(void * addr, void * phyAddr, NX_Size length, NX_U32 prot)
{
    void * mappedAddr;
    NX_Error err;
    
    mappedAddr = (void *)NX_Syscall5(NX_API_MemMap2, addr, phyAddr, length, prot, &err);
    NX_ErrorSet(err);

    return mappedAddr;
}

NX_Error NX_MemUnmap(void * addr, NX_Size length)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_MemUnmap, addr, length)));
    return err;
}

void * NX_MemHeap(void * addr)
{
    void * mappedAddr;
    NX_Error err;
    
    mappedAddr = (void *)NX_Syscall2(NX_API_MemHeap, addr, &err);
    NX_ErrorSet(err);

    return mappedAddr;
}
