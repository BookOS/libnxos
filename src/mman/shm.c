#include <nxos/mman.h>
#include <nxos/syscall.h>

NX_Solt NX_ShareMemOpen(const char * name, NX_Size size, NX_U32 flags)
{
    NX_Error err;
    NX_Solt solt = NX_SOLT_INVALID_VALUE;
    NX_ErrorSet((err = NX_Syscall4(NX_API_ShareMemOpen, name, size, flags, &solt)));
    return solt;
}

void * NX_ShareMemMap(NX_Solt shmSolt, NX_Solt * outSolt)
{
    NX_Error err;
    void * mapAddr = NX_NULL;
    NX_Solt solt = NX_SOLT_INVALID_VALUE;

    NX_ErrorSet((err = NX_Syscall3(NX_API_ShareMemMap, shmSolt, &mapAddr, &solt)));
    if (outSolt)
    {
        *outSolt = solt;
    }
    return mapAddr;
}
