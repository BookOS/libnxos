/**
 * Copyright (c) 2018-2023, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: atomic
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2023-08-23     JasonHu           Init
 */

#ifndef __NXOS_ATOMIC_H__
#define __NXOS_ATOMIC_H__

#include <nxos/types.h>
#include <nxos/defines.h>
#include <nxos/error.h>

typedef NX_IArch NX_AtomicValue;

struct NX_Atomic
{
    NX_VOLATILE NX_AtomicValue value;
};
typedef struct NX_Atomic NX_Atomic;

#define NX_ATOMIC_INIT_VALUE(val) {val}
#define NX_ATOMIC_DEFINE(name, val) NX_Atomic name = NX_ATOMIC_INIT_VALUE(val);

void NX_AtomicSet(NX_Atomic *atomic, NX_AtomicValue value);
NX_AtomicValue NX_AtomicGet(NX_Atomic *atomic);
void NX_AtomicAdd(NX_Atomic *atomic, NX_AtomicValue value);
void NX_AtomicSub(NX_Atomic *atomic, NX_AtomicValue value);
void NX_AtomicInc(NX_Atomic *atomic);
void NX_AtomicDec(NX_Atomic *atomic);
void NX_AtomicSetMask(NX_Atomic *atomic, NX_AtomicValue mask);
void NX_AtomicClearMask(NX_Atomic *atomic, NX_AtomicValue mask);
NX_AtomicValue NX_AtomicSwap(NX_Atomic *atomic, NX_AtomicValue newValue);
NX_AtomicValue NX_AtomicCAS(NX_Atomic *atomic, NX_AtomicValue old, NX_AtomicValue newValue);

#endif /* __NXOS_ATOMIC_H__ */
