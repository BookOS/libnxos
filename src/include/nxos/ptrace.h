#ifndef __NXOS_PTRACE_H__
#define __NXOS_PTRACE_H__


enum __ptrace_request{

  PTRACE_PEEKINT8 = 0,

  PTRACE_PEEKINT16 = 1,

  PTRACE_PEEKINT32 = 2,

  PTRACE_PEEKINT64 = 3,

  PTRACE_POKEINT8 = 4,

  PTRACE_POKEINT16 = 5,

  PTRACE_POKEINT32 = 6,

  PTRACE_POKEINT64 = 7,

  /* Continue the process.  */
  PTRACE_CONT = 8,

  /* Kill the process.  */
  PTRACE_KILL = 9,

  /* Single step the process.  */
  PTRACE_SINGLESTEP = 10,

  /* Get all general purpose registers used by a processes.  */
  PTRACE_GETREGS = 11,

  /* Set all general purpose registers used by a processes.  */
  PTRACE_SETREGS = 12,

  /* Get all floating point registers used by a processes.  */
  PTRACE_GETFPREGS = 13,

  /* Set all floating point registers used by a processes.  */
  PTRACE_SETFPREGS = 14,

};

#endif