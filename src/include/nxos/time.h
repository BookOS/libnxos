/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: time api
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-05-14     JasonHu           Init
 */

#ifndef __NXOS_TIME_H__
#define __NXOS_TIME_H__

#include <nxos/xbook.h>

#ifdef __cplusplus
extern "C" {
#endif

#define NX_TIME_YEAR_BASE 1900

typedef NX_U32 NX_TimeStamp;

typedef struct {
    NX_U8 second;       /* [0-59] */
    NX_U8 minute;       /* [0-59] */
    NX_U8 hour;         /* [0-23] */
    NX_U8 weekDay;      /* [0-6] */
    NX_U32 day;         /* [1-31] */
    NX_U32 month;       /* [0-11] */
    NX_U32 year;        /* year, offset of 1900 */
    NX_U32 yearDay;     /* [0-366] */
} NX_Time;

NX_Error NX_TimeSet(NX_Time * time);
NX_Error NX_TimeGet(NX_Time * time);

int NX_TimeStampToTime(NX_TimeStamp t, NX_Time * tm);
NX_TimeStamp NX_TimeToTimeStamp(const NX_Time * tm);

#ifdef __cplusplus
}
#endif

#endif  /* __NXOS_TIME_H__ */
