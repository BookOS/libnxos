/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: virtual file system
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-4-1       JasonHu           Init
 */

#ifndef __NXOS_FILE_SYSTEM_H__
#define __NXOS_FILE_SYSTEM_H__

#include <nxos/xbook.h>
#include <nxos/process.h>

#ifdef __cplusplus
extern "C" {
#endif

#define NX_FILE_MAX_PATH		    (1024)
#define NX_FILE_MAX_NAME		    (256)

#define NX_FILE_RDONLY			(1 << 0)
#define NX_FILE_WRONLY			(1 << 1)
#define NX_FILE_RDWR			(NX_FILE_RDONLY | NX_FILE_WRONLY)
#define NX_FILE_ACCMODE		(NX_FILE_RDWR)

#define NX_FILE_CREAT			(1 << 8)
#define NX_FILE_EXCL			(1 << 9)
#define NX_FILE_TRUNC			(1 << 11)
#define NX_FILE_APPEND			(1 << 12)

#define NX_FILE_MODE_EXEC			(1 << 6)
#define NX_FILE_MODE_WRITE			(1 << 7)
#define NX_FILE_MODE_READ			(1 << 8)
#define NX_FILE_MODE_MASK			(NX_FILE_MODE_READ | NX_FILE_MODE_WRITE | NX_FILE_MODE_EXEC)

#define	NX_FILE_TYPE_DIR			(1 << 16)
#define	NX_FILE_TYPE_REG			(1 << 19)
#define	NX_FILE_TYPE_MASK			(NX_FILE_TYPE_DIR | NX_FILE_TYPE_REG)

#define NX_FILE_IS_DIR(m)		((m) & NX_FILE_TYPE_DIR )
#define NX_FILE_IS_REG(m)		((m) & NX_FILE_TYPE_REG )

#define	NX_FILE_READ_OK				(1 << 2)
#define NX_FILE_WRITE_OK				(1 << 1)
#define NX_FILE_EXEC_OK				(1 << 0)

#define NX_FILE_SEEK_SET		    (0)
#define NX_FILE_SEEK_CUR		    (1)
#define NX_FILE_SEEK_END		    (2)

typedef struct NX_FileStatInfo
{
	NX_Size size;
	NX_U32 mode;
	NX_U32 ctime;
	NX_U32 atime;
	NX_U32 mtime;
} NX_FileStatInfo;

enum NX_DirentType
{
	NX_DIR_TYPE_UNK,
	NX_DIR_TYPE_DIR,
	NX_DIR_TYPE_REG,
};

typedef struct NX_Dirent
{
	NX_Offset off;
	NX_U32 reclen;
	enum NX_DirentType type;
	char name[NX_FILE_MAX_NAME];
} NX_Dirent;

NX_Error NX_FileSystemMount(const char * dev, const char * dir, const char * fsname, NX_U32 flags);
NX_Error NX_FileSystemUnmount(const char * path);
NX_Error NX_FileSystemSync(void);
NX_Solt NX_FileOpen(const char * path, NX_U32 flags, NX_U32 mode);
NX_Error NX_FileClose(NX_Solt solt);
NX_Size NX_FileRead(NX_Solt solt, void * buf, NX_Size len);
NX_Size NX_FileWrite(NX_Solt solt, void * buf, NX_Size len);
NX_Error NX_FileIoctl(NX_Solt solt, NX_U32 cmd, void *arg);
NX_Offset NX_FileSetPointer(NX_Solt solt, NX_Offset off, int whence);
NX_Error NX_FileSync(NX_Solt solt);
NX_Error NX_FileSetMode(NX_Solt solt, NX_U32 mode);
NX_Error NX_FileGetStat(NX_Solt solt, NX_FileStatInfo * st);
NX_Solt NX_DirOpen(const char * name);
NX_Error NX_DirClose(NX_Solt solt);
NX_Error NX_DirRead(NX_Solt solt, NX_Dirent * dir);
NX_Error NX_DirResetPointer(NX_Solt solt);
NX_Error NX_DirCreate(const char * path, NX_U32 mode);
NX_Error NX_DirDelete(const char * path);
NX_Error NX_FileRename(const char * src, const char * dst);
NX_Error NX_FileDelete(const char * path);
NX_Error NX_FileAccess(const char * path, NX_U32 mode);
NX_Error NX_FileSetModeToPath(const char * path, NX_U32 mode);
NX_Error NX_FileGetStatFromPath(const char * path, NX_FileStatInfo * st);

#ifdef __cplusplus

}
#endif

#endif  /* __NXOS_FILE_SYSTEM_H__ */
