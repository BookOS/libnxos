/**
 * Copyright (c) 2018-2024, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: message queue api
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2024-10-04     JasonHu           Init
 */

#ifndef __NXOS_MSGQUEUE_H__
#define __NXOS_MSGQUEUE_H__

#include <nxos/xbook.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct NX_MsgBuf
{
    char *payload;
    NX_Size len;
} NX_MsgBuf;

NX_Solt NX_MsgQueueCreate(NX_Size msgSize, NX_Size msgCount);
#define NX_MsgQueueDestroy(msgQueue) NX_SoltClose(msgQueue)

NX_Error NX_MsgQueueSend(NX_Solt msgQueue, NX_MsgBuf *buf);
NX_Error NX_MsgQueueTrySend(NX_Solt msgQueue, NX_MsgBuf *buf);
NX_Error NX_MsgQueueRecv(NX_Solt msgQueue, NX_MsgBuf *buf);
NX_Error NX_MsgQueueTryRecv(NX_Solt msgQueue, NX_MsgBuf *buf);
NX_Bool NX_MsgQueueFull(NX_Solt msgQueue);
NX_Bool NX_MsgQueueEmpty(NX_Solt msgQueue);
NX_Size NX_MsgQueueLenght(NX_Solt msgQueue);

#ifdef __cplusplus
}
#endif

#endif  /* __NXOS_MSGQUEUE_H__ */
