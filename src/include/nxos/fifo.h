/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: fifo buffer
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-06-19     JasonHu           Init
 */

#ifndef __NXOS_FIFO_H__
#define __NXOS_FIFO_H__

#include <nxos/xbook.h>
#include <nxos/process.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct NX_UserFifo
{
    NX_U8 * buffer;
    NX_Size size;
    NX_Size in;
    NX_Size out;
    NX_Solt lock; /* mutex lock */
} NX_UserFifo;

NX_Error NX_UserFifoInit(NX_UserFifo * fifo, NX_U8 * buffer, NX_Size size);
NX_UserFifo * NX_UserFifoCreate(NX_Size size);
void NX_UserFifoDestroy(NX_UserFifo * fifo);

NX_Size NX_UserFifoPut(NX_UserFifo * fifo, const NX_U8 * buffer, NX_Size len);
NX_Size NX_UserFifoGet(NX_UserFifo * fifo, const NX_U8 * buffer, NX_Size len);

void NX_UserFifoReset(NX_UserFifo *fifo);
NX_Size NX_UserFifoLen(NX_UserFifo *fifo);
NX_Size NX_UserFifoAvaliable(NX_UserFifo *fifo);

NX_Solt NX_FifoCreate(NX_Size size);
#define NX_FifoDestroy(fifo) NX_SoltClose(fifo)

NX_Size NX_FifoWrite(NX_Solt fifo, const NX_U8 * buffer, NX_Size len);
NX_Size NX_FifoRead(NX_Solt fifo, const NX_U8 * buffer, NX_Size len);

NX_Size NX_FifoLength(NX_Solt fifo);

#ifdef __cplusplus
}
#endif

#endif  /* __NXOS_FIFO_H__ */
