/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: snapshot api
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-10-26     JasonHu           Init
 */

#ifndef __NXOS_SNAPSHOT_H__
#define __NXOS_SNAPSHOT_H__

#include <nxos/xbook.h>
#include <nxos/process.h>
#include <nxos/fs.h>
#include <nxos/perf.h>

#ifdef __cplusplus
extern "C" {
#endif

#define NX_SNAPSHOT_PROCESS 1
#define NX_SNAPSHOT_THREAD  2
#define NX_SNAPSHOT_MAX     3

#define NX_THREAD_NAME_LEN 32

typedef struct NX_VmspaceMap
{
    NX_Addr spaceBase;   /* user space area */
    NX_Addr spaceTop;
    NX_Addr imageStart;   /* image: code & data */
    NX_Addr imageEnd;
    NX_Addr heapStart;
    NX_Addr heapEnd;
    NX_Addr heapCurrent; /* current heap: must page aligned */
    NX_Addr mapStart;
    NX_Addr mapEnd;
    NX_Size mapSize;
    NX_Addr stackStart;
    NX_Addr stackEnd;
    NX_Addr stackBottom; /* current stack bottom */
} NX_VmspaceMapInfo;

typedef struct NX_SnapshotThread
{
    NX_UsageInfo usage;
    NX_U32 state;
    NX_U32 threadId;
    NX_U32 ownerProcessId;
    NX_U32 fixedPriority;
    NX_U32 priority;
    NX_U32 onCore;        /* thread on which core */
    NX_U32 coreAffinity;  /* thread would like to run on the core */
    NX_U32 flags;
    char name[NX_THREAD_NAME_LEN];
} NX_SnapshotThread;

typedef struct NX_SnapshotProcess
{
    NX_UsageInfo usage;
    NX_U32 processId;
    NX_U32 threadCount;
    NX_U32 parentProcessId; 
    NX_U32 flags;
    NX_VmspaceMapInfo map;
    char exePath[NX_FILE_MAX_PATH]; /* execute path */
} NX_SnapshotProcess;

NX_Solt NX_SnapshotCreate(NX_U32 snapshotType, NX_U32 flags);
NX_Error NX_SnapshotFirst(NX_Solt solt, void * object);
NX_Error NX_SnapshotNext(NX_Solt solt, void * object);

#ifdef __cplusplus
}
#endif

#endif  /* __NXOS_SNAPSHOT_H__ */
