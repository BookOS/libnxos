/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: process api
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-3-19      JasonHu           Init
 */

#ifndef __NXOS_HUB_H__
#define __NXOS_HUB_H__

#include <nxos/xbook.h>

#ifdef __cplusplus
extern "C" {
#endif

#define NX_HUB_WAIT     0x00 /* hub wait block */
#define NX_HUB_NOWAIT   0x01 /* hub wait noblock */

typedef void * NX_HubHandler;

#define NX_HUB_PARAM_NR 8
typedef struct NX_HubParam
{
    NX_U32 api;
    NX_Size args[NX_HUB_PARAM_NR];
} NX_HubParam;

NX_Error NX_HubRegister(const char *name, NX_Size maxClient);
NX_Error NX_HubUnregister(const char *name);
NX_Error NX_HubCallParamName(const char *name, NX_HubParam *param, NX_Size *retVal);
NX_Error NX_HubReturn(NX_Size retVal, NX_Error retErr);
NX_Error NX_HubWait(NX_HubParam *param, NX_U32 flags);
void *NX_HubTranslate(void *addr, NX_Size size);

NX_Error NX_HubDispatch(NX_HubParam *param, NX_HubHandler handlerTable[], NX_Size tableSize);
NX_Error NX_HubRun(NX_HubHandler handlerTable[], NX_Size tableSize, NX_U32 flags);

#ifdef __cplusplus
}
#endif

#endif  /* __NXOS_HUB_H__ */
