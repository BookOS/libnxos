/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: system perf
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2023-09-17     JasonHu           Init
 */

#ifndef __SYSTEM_PERF_H__
#define __SYSTEM_PERF_H__

#include <nxos/xbook.h>
#include <nxos/clock.h>

typedef NX_TimeVal NX_PerfTime;

typedef struct NX_CpuUsageInfo {
    NX_PerfTime user;
    NX_PerfTime system;
    NX_PerfTime delayirq;
    NX_PerfTime irq;
    NX_PerfTime idle;
    NX_PerfTime iowait;
} NX_CpuUsageInfo;

typedef struct NX_UsageInfo {
    NX_PerfTime user;
    NX_PerfTime system;
} NX_UsageInfo;

#endif  /* __SYSTEM_PERF_H__ */
