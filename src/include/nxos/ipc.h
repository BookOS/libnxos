/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: ipc api
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-11-11     JasonHu           Init
 */

#ifndef __NXOS_IPC_H__
#define __NXOS_IPC_H__

#include <nxos/xbook.h>
#include <nxos/process.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct NX_IpcMsg
{
    NX_Solt clientSlot;
    NX_Size dataLen;
    NX_Size dataOffset;
    NX_Size soltCount;
    NX_Size capSlotsOffset;
} NX_IpcMsg;

typedef void (*NX_ServerHandler)(NX_IpcMsg * ipc_msg);

typedef struct NX_IpcClient
{
    NX_Solt connect;
    NX_Addr sharedBuf;
    NX_Size sharedBufLen;
} NX_IpcClient;

typedef struct 
{
    NX_Addr bufAddr;
    NX_Size bufSize;
} NX_IpcClientConfig;

NX_Solt NX_IpcBind(const char *name, NX_ServerHandler handler, NX_Size maxClient);
NX_Error NX_IpcConnect(const char *name, NX_IpcClient * client, NX_Size bufSize);
NX_Size NX_IpcCall(NX_IpcClient * client, NX_IpcMsg * ipcMsg);
NX_Error NX_IpcReturn(NX_Size returnValue);

char * NX_IpcGetMsgData(NX_IpcMsg *ipcMsg);
NX_Solt NX_IpcGetMsgSolt(NX_IpcMsg *ipc_msg, NX_Offset index);
NX_Error NX_IpcSetMsgSolt(NX_IpcMsg * ipc_msg, NX_Offset index, NX_Solt solt);
NX_IpcMsg * NX_IpcCreateMsg(NX_IpcClient * client, NX_Size dataLen, NX_Size soltCount);
NX_Error NX_IpcDestroyMsg(NX_IpcMsg * ipc_msg);

#ifdef __cplusplus
}
#endif

#endif  /* __NXOS_IPC_H__ */
