/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: simple math lib without float
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-05-22     JasonHu           Init
 */

#ifndef __NXOS_MATH_H__
#define __NXOS_MATH_H__

#include <nxos/xbook.h>
#include <nxos/bitops.h>

#ifdef __cplusplus
extern "C" {
#endif

NX_INLINE int NX_PowInt(int x, int n)
{
    int res = 1;
    if (n < 0)
    {
        x = 1 / x;
        n = -n;
    }
    while (n)
    {
        if(n & 1)
        {
            res *= x;
        }
        x *= x;
        n >>= 1;
    }
    return res;
}

#define NX_IsPowerOf2(n) (n != 0 && ((n & (n - 1)) == 0))

NX_INLINE NX_Size NX_RoundupPowOf2(NX_Size x)
{
    return 1UL << NX_FLS(x - 1);
}

#ifdef __cplusplus
}
#endif

#endif  /* __NXOS_MATH_H__ */
