/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: xbook basic header
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-3-19      JasonHu           Init
 */

#ifndef __NXOS_XBOOK_H__
#define __NXOS_XBOOK_H__

#if !defined(__ASSEMBLY__) && !defined(ASSEMBLY) 
#include <nxos/defines.h>
#include <nxos/types.h>
#include <nxos/error.h>
#endif

#endif  /* __NXOS_XBOOK_H__ */
