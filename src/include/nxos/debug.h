/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: debug api
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-3-19      JasonHu           Init
 */

#ifndef __NXOS_DEBUG_H__
#define __NXOS_DEBUG_H__

#include <nxos/xbook.h>
#include <nxos/ptrace.h>
#ifdef __cplusplus
extern "C" {
#endif

NX_Error NX_DebugLog(char *buf, NX_Size len);
NX_Error NX_Ptrace(enum __ptrace_request request, NX_I32 tid, void *addr, void *data);

#ifdef __cplusplus
}
#endif

#endif  /* __NXOS_DEBUG_H__ */
