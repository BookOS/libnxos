/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: sys info header
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-05-13     JasonHu           Init
 */

#ifndef __NXOS_SYSINFO_H__
#define __NXOS_SYSINFO_H__

#include <nxos/xbook.h>
#include <nxos/perf.h>

#ifdef __cplusplus
extern "C" {
#endif

#define NX_CPUINFO_GET_CORES 1

#define NX_MAX_CPU  32

typedef struct NX_MemInfo
{
    NX_Size pageSize;
    NX_Size totalPage;
    NX_Size usedPage;
    NX_Size memPage;
} NX_MemInfo;

typedef struct NX_CpuInfo
{
    NX_U32 cores;
} NX_CpuInfo;

#ifdef __cplusplus
}
#endif

#endif  /* __NXOS_SYSINFO_H__ */
