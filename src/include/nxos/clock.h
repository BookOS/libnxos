/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: clock api
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-05-13     JasonHu           Init
 */

#ifndef __NXOS_CLOCK_H__
#define __NXOS_CLOCK_H__

#include <nxos/xbook.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef NX_UArch NX_TimeVal;
typedef NX_UArch NX_ClockTick;

NX_TimeVal NX_ClockGetMillisecond(void);

#ifdef __cplusplus
}
#endif

#endif  /* __NXOS_CLOCK_H__ */
