/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: pipe
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2024-11-12     JasonHu           Init
 */

#ifndef __NXOS_PIPE_H__
#define __NXOS_PIPE_H__

#include <nxos/xbook.h>

#ifdef __cplusplus
extern "C" {
#endif

#define NX_PIPE_SEND        0x01
#define NX_PIPE_RECV        0x02
#define NX_PIPE_NOWAIT      0x04

NX_Solt NX_PipeCreate(const char *name, NX_Size size, NX_U8 flags);
NX_Solt NX_PipeConnect(const char *name);
NX_Error NX_PipeDisconnect(NX_Solt solt);
NX_Size NX_PipeSend(NX_Solt solt, void *buffer, NX_Size len);
NX_Size NX_PipeRecv(NX_Solt solt, void *buffer, NX_Size len);

#ifdef __cplusplus
}
#endif

#endif  /* __NXOS_PIPE_H__ */
