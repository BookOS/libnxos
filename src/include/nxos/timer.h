/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: timer api
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-05-13     JasonHu           Init
 */

#ifndef __NXOS_TIMER_H__
#define __NXOS_TIMER_H__

#include <nxos/xbook.h>
#include <nxos/process.h>

#ifdef __cplusplus
extern "C" {
#endif

#define NX_TIMER_ONESHOT        0x01    /* timer type is one shot */
#define NX_TIMER_PERIOD         0x02    /* timer type is period */
#define NX_TIMER_CREATE_START   0x10    /* timer create with start */

NX_Solt NX_TimerCreate(NX_UArch milliseconds, void (*handler)(void *arg), void *arg, NX_U32 flags);
NX_Error NX_TimerStart(NX_Solt solt);
NX_Error NX_TimerStop(NX_Solt solt);

#ifdef __cplusplus
}
#endif

#endif  /* __NXOS_TIMER_H__ */
