/**
 * Copyright (c) 2018-2023, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: spin lock
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2023-08-23     JasonHu           Init
 */

#ifndef __NXOS_SPIN_H__
#define __NXOS_SPIN_H__

#include <nxos/atomic.h>

#define NX_SPIN_LOCK_VALUE 1

struct NX_Spin
{
    NX_Atomic value;
};
typedef struct NX_Spin NX_Spin;

#define NX_SPIN_DEFINE_UNLOCKED(name) NX_Spin name = {NX_ATOMIC_INIT_VALUE(0)}
#define NX_SPIN_DEFINE_LOCKED(name) NX_Spin name = {NX_ATOMIC_INIT_VALUE(1)}

NX_Error NX_SpinInit(NX_Spin *lock);
NX_Error NX_SpinTryLock(NX_Spin *lock);
NX_Error NX_SpinLock(NX_Spin *lock);
NX_Error NX_SpinUnlock(NX_Spin *lock);
NX_Error NX_SpinLockIRQ(NX_Spin *lock, NX_UArch *level);
NX_Error NX_SpinTryLockIRQ(NX_Spin *lock, NX_UArch *level);
NX_Error NX_SpinUnlockIRQ(NX_Spin *lock, NX_UArch level);
NX_Bool NX_SpinIsLocked(NX_Spin *lock);

#endif /* __NXOS_SPIN_H__ */
