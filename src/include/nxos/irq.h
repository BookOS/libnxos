/**
 * Copyright (c) 2018-2023, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: interrupt request
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2023-08-23     JasonHu           Init
 */

#ifndef __NXOS_IRQ_H__
#define __NXOS_IRQ_H__

#include <nxos/types.h>

/**
 * do not support irq in user mode
 */
#define NX_IRQ_Enable()            
#define NX_IRQ_Disable()           
#define NX_IRQ_SaveLevel()         (0)
#define NX_IRQ_RestoreLevel(level) 

#endif  /* __NXOS_IRQ_H__ */
