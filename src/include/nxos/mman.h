/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: memory management api
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-4-20      JasonHu           Init
 */

#ifndef __NXOS_MMAN_H__
#define __NXOS_MMAN_H__

#include <nxos/xbook.h>
#include <nxos/process.h>

#ifdef __cplusplus
extern "C" {
#endif

#define NX_PROT_READ    0x01    /* mem readable */
#define NX_PROT_WRITE   0x02    /* mem writable */
#define NX_PROT_EXEC    0x04    /* mem executeable */

void * NX_MemMap(void * addr, NX_Size length, NX_U32 prot);
void * NX_MemMap2(void * addr, void * phyAddr, NX_Size length, NX_U32 prot);
NX_Error NX_MemUnmap(void * addr, NX_Size length);
void * NX_MemHeap(void * addr);

int NX_PosixBrk(void * addr);
void * NX_PosixSbrk(int increment);

void * NX_MemAlloc(NX_Size size);
void * NX_MemAllocEx(NX_Size number, NX_Size size);
void * NX_MemReAlloc(void * p, NX_Size size);
void * NX_MemAlignedAlloc(NX_Size alignment, NX_Size size);
void NX_MemFree(void * ptr);

#define NX_SHAREMEM_NAME_LEN 32

#define NX_SHAREMEM_CREATE_NEW  0x01 /* create share memory new */
#define NX_SHAREMEM_ANONYMOUS   0x02 /* create share memory anonymous */

NX_Solt NX_ShareMemOpen(const char * name, NX_Size size, NX_U32 flags);
void * NX_ShareMemMap(NX_Solt shmSolt, NX_Solt * outSolt);
#define NX_ShareMemClose(solt) NX_SoltClose(solt)
#define NX_ShareMemUnmap(solt) NX_SoltClose(solt)

#ifdef __cplusplus
}
#endif

#endif  /* __NXOS_MMAN_H__ */
