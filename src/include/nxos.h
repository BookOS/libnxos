/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: next xbook base lib header file, include all header files.
 *           This file should never include in lib, only for user.
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-2-9       JasonHu           Init
 */

#ifndef __NXOS_ALL_H__
#define __NXOS_ALL_H__

#include <nxos/xbook.h>
#include <nxos/syscall.h>
#include <nxos/process.h>
#include <nxos/debug.h>
#include <nxos/crt.h>
#include <nxos/utils.h>
#include <nxos/list.h>
#include <nxos/assert.h>
#include <nxos/fs.h>
#include <nxos/hub.h>
#include <nxos/mman.h>
#include <nxos/clock.h>
#include <nxos/sysinfo.h>
#include <nxos/time.h>
#include <nxos/device.h>
#include <nxos/signal.h>
#include <nxos/timer.h>
#include <nxos/drvfw.h>
#include <nxos/bitops.h>
#include <nxos/fifo.h>
#include <nxos/math.h>
#include <nxos/snapshot.h>
#include <nxos/ipc.h>
#include <nxos/atomic.h>
#include <nxos/spin.h>
#include <nxos/initcall.h>
#include <nxos/msgqueue.h>
#include <nxos/pipe.h>

#endif /* __NXOS_ALL_H__ */
