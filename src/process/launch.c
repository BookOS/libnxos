#include <nxos/syscall.h>
#include <nxos/process.h>

NX_Solt NX_ProcessLaunch(char *path, NX_U32 flags, NX_U32 *exitCode, char *cmd, char *env)
{
    NX_Solt solt = NX_SOLT_INVALID_VALUE;
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall6(NX_API_ProcessLaunch, path, flags, exitCode, cmd, env, &solt)));
    return solt;
}
