/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: solt close
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-05-03     JasonHu           Init
 */

#include <nxos/syscall.h>
#include <nxos/process.h>

NX_Error NX_SoltClose(NX_Solt solt)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall1(NX_API_SoltClose, solt)));
    return err;
}

NX_Solt NX_SoltCopy(NX_Solt dest, NX_Solt solt)
{
    NX_Solt newSolt = NX_SOLT_INVALID_VALUE;
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall3(NX_API_SoltCopy, dest, solt, &newSolt)));
    return newSolt;
}

NX_Solt NX_SoltCopyTo(NX_Solt destThread, NX_Solt srcSolt, NX_Solt destSolt)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall3(NX_API_SoltCopyTo, destThread, srcSolt, destSolt)));
    if (err != NX_EOK)
    {
        return NX_SOLT_INVALID_VALUE;
    }
    return destSolt;
}

NX_Solt NX_SoltInstall(NX_Solt solt, void *object)
{
    NX_Solt newSolt = NX_SOLT_INVALID_VALUE;
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall3(NX_API_SoltInstall, solt, object, &newSolt)));
    if (err != NX_EOK)
    {
        return NX_SOLT_INVALID_VALUE;
    }
    return newSolt;
}

NX_Error NX_SoltUninstall(NX_Solt dest, NX_Solt solt)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_SoltUninstall, dest, solt)));
    return err;
}

void *NX_SoltObject(NX_Solt dest, NX_Solt solt)
{
    NX_Error err;
    NX_UArch object = 0;
    NX_ErrorSet((err = NX_Syscall3(NX_API_SoltObject, dest, solt, &object)));
    return (void *)object;
}
