/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: thread local storage
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-05-20     JasonHu           Init
 */

#include <nxos/process.h>

typedef struct NX_TlsArea
{
    struct NX_TlsArea * tlsAreaSelf;    /* tls area self */
    NX_Error error; /* error in tls, save current thread error number */
    void * extensionData; /* tls extension data */
    /* tls data area */
    void * tlsData[NX_TLS_MAX_NR];
} NX_TlsArea;

NX_PRIVATE NX_U8 AllocBitmap[NX_TLS_MAX_NR / 8] = {0}; /* bitmap for tls index allocate */
NX_PRIVATE NX_Solt AllocBitmapLock = NX_SOLT_INVALID_VALUE;

NX_IMPORT void * __NX_TlsGetArea(void);

int NX_TlsAlloc(void)
{
    int i;

    if (AllocBitmapLock == NX_SOLT_INVALID_VALUE)
    {
        AllocBitmapLock = NX_MutexCreate(0);
        if (AllocBitmapLock == NX_SOLT_INVALID_VALUE)
        {
            return -1;
        }
    }

    NX_MutexAcquire(AllocBitmapLock);
    for (i = 0; i < NX_TLS_MAX_NR; i++)
    {
        if (!(AllocBitmap[i / 8] & (1 << (i % 8))))
        {
            AllocBitmap[i / 8] |= (1 << (i % 8));
            NX_MutexRelease(AllocBitmapLock);
            return i;
        }
    }
    NX_MutexRelease(AllocBitmapLock);
    return -1;
}

NX_Error NX_TlsFree(int index)
{
    if (index >= NX_TLS_MAX_NR || index < 0)
    {
        return NX_EINVAL;
    }

    if (AllocBitmapLock == NX_SOLT_INVALID_VALUE)
    {
        AllocBitmapLock = NX_MutexCreate(0);
        if (AllocBitmapLock == NX_SOLT_INVALID_VALUE)
        {
            return NX_ENOMEM;
        }
    }

    NX_MutexAcquire(AllocBitmapLock);
    AllocBitmap[index / 8] &= ~(1 << (index % 8));
    NX_MutexRelease(AllocBitmapLock);
    return NX_EOK;
}

NX_Error NX_TlsSetValue(int index, void * value)
{
    NX_TlsArea * tlsArea = (void *)__NX_TlsGetArea();

    if (index >= NX_TLS_MAX_NR || index < 0)
    {
        return NX_EINVAL;
    }
    tlsArea->tlsData[index] = value;
    return NX_EOK;
}

void * NX_TlsGetValue(int index)
{
    NX_TlsArea * tlsArea = (void *)__NX_TlsGetArea();

    if (index >= NX_TLS_MAX_NR || index < 0)
    {
        return NX_NULL;
    }
    return tlsArea->tlsData[index];
}

/**
 * error saved in tls, each thread get error from tls
 */
NX_Error * NX_ErrorGetLocation(void)
{
    NX_TlsArea * tlsArea = (void *)__NX_TlsGetArea();
    return &tlsArea->error;
}

void NX_TlsSetExtension(void * data)
{
    NX_TlsArea * tlsArea = (void *)__NX_TlsGetArea();
    tlsArea->extensionData = data;
}

void * NX_TlsGetExtension(void)
{
    NX_TlsArea * tlsArea = (void *)__NX_TlsGetArea();
    return tlsArea->extensionData;
}

void * NX_TlsGetExtensionAddr(void)
{
    NX_TlsArea * tlsArea = (void *)__NX_TlsGetArea();
    return &tlsArea->extensionData;
}

void NX_TlsClearData(void)
{
    NX_TlsArea * tlsArea = (void *)__NX_TlsGetArea();
    NX_MemSet(tlsArea->tlsData, 0, sizeof(tlsArea->tlsData));
}
