#include <nxos/process.h>

NX_PRIVATE NX_ExitCallHandler exit_call_table[NX_EXIT_CALL_NR] = {NX_NULL};

NX_Error NX_ExitCallRegister(NX_ExitCallHandler handler)
{
    int i;
    for (i = 0; i < NX_EXIT_CALL_NR; i++)
    {
        if (!exit_call_table[i])
        {
            exit_call_table[i] = handler;
            return NX_EOK;
        }
    }
    return NX_ENORES;
}

void NX_ExitCallHandlerInvoke(void)
{
    int i;
    for (i = NX_EXIT_CALL_NR - 1; i >= 0; i--)
    {
        if (exit_call_table[i])
        {
            exit_call_table[i]();
        }
    }
}
