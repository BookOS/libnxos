#include <nxos/syscall.h>
#include <nxos/initcall.h>

NX_IMPORT void NX_ExitCallHandlerInvoke(void);

void NX_ProcessExit(NX_U32 exitCode)
{
    NX_ExitCallInvoke();
    NX_ExitCallHandlerInvoke();
    NX_Syscall(NX_API_ProcessExit, exitCode);
}
