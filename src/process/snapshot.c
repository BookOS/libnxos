/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: solt close
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-05-03     JasonHu           Init
 */

#include <nxos/syscall.h>
#include <nxos/snapshot.h>

NX_Solt NX_SnapshotCreate(NX_U32 snapshotType, NX_U32 flags)
{
    NX_Solt solt;
    NX_Error err = NX_EOK;

    solt = NX_Syscall3(NX_API_SnapshotCreate, snapshotType, flags, &err);
    NX_ErrorSet(err);
    return solt;
}

NX_Error NX_SnapshotFirst(NX_Solt solt, void * object)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_SnapshotFirst, solt, object)));
    return err;
}

NX_Error NX_SnapshotNext(NX_Solt solt, void * object)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_SnapshotNext, solt, object)));
    return err;
}
