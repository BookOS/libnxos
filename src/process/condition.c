/**
 * Copyright (c) 2018-2023, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: Condition variable
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2023-08-26     JasonHu           Init
 */

#include <nxos/syscall.h>
#include <nxos/process.h>
#include <nxos/error.h>

NX_Solt NX_ConditionCreate(NX_U32 attr)
{
    NX_Solt solt = NX_SOLT_INVALID_VALUE;
    NX_Error err;

    NX_ErrorSet((err = NX_Syscall2(NX_API_ConditionCreate, &solt, attr)));
    return solt;
}

NX_Error NX_ConditionWait(NX_Solt solt, NX_Solt mutexSolt)
{
    NX_Error err;

    NX_ErrorSet((err = NX_Syscall2(NX_API_ConditionWait, solt, mutexSolt)));
    return err;
}

NX_Error SysConditionWaitTimeout(NX_Solt solt, NX_Solt mutexSolt, NX_TimeVal tv)
{
    NX_Error err;

    NX_ErrorSet((err = NX_Syscall3(NX_API_ConditionWaitTimeout, solt, mutexSolt, tv)));
    return err;
}

NX_Error NX_ConditionSignal(NX_Solt solt)
{
    NX_Error err;

    NX_ErrorSet((err = NX_Syscall1(NX_API_ConditionSignal, solt)));
    return err;
}

NX_Error NX_ConditionBroadcast(NX_Solt solt)
{
    NX_Error err;

    NX_ErrorSet((err = NX_Syscall1(NX_API_ConditionBroadcast, solt)));
    return err;
}
