/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: device api
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-05-27     JasonHu           Init
 */

#include <nxos/syscall.h>
#include <nxos/device.h>
#include <nxos/mman.h>
#include <nxos/utils.h>
#include <nxos/assert.h>

NX_Solt NX_DriverRegister(NX_Driver *driver)
{
    NX_Solt solt = NX_SOLT_INVALID_VALUE;
    NX_ErrorSet(NX_Syscall2(NX_API_DriverRegister, driver, &solt));
    return solt;
}

NX_PRIVATE NX_Error UdriverOpen(NX_Driver *driver, struct NX_Device *device, NX_U32 flags)
{
    NX_Error err = NX_EOK;
    if (driver->ops->open)
    {
        err = driver->ops->open(device, flags);
    }
    return err;
}

NX_PRIVATE NX_Error UdriverClose(NX_Driver *driver, struct NX_Device *device)
{
    NX_Error err = NX_EOK;
    if (driver->ops->close)
    {
        err = driver->ops->close(device);
    }
    return err;
}

NX_PRIVATE NX_Error UdriverRead(NX_Driver *driver, struct NX_Device *device, void *buf, NX_Offset off, NX_Size len, NX_Size *outLen)
{
    NX_Error err = NX_ENOFUNC;
    if (driver->ops->read)
    {
        err = driver->ops->read(device, buf, off, len, outLen);
    }
    return err;
}

NX_PRIVATE NX_Error UdriverWrite(NX_Driver *driver, struct NX_Device *device, void *buf, NX_Offset off, NX_Size len, NX_Size *outLen)
{
    NX_Error err = NX_ENOFUNC;
    if (driver->ops->write)
    {
        err = driver->ops->write(device, buf, off, len, outLen);
    }
    return err;
}

NX_PRIVATE NX_Error UdriverControl(NX_Driver *driver, struct NX_Device *device, NX_U32 cmd, void *arg)
{
    NX_Error err = NX_ENOFUNC;
    if (driver->ops->control)
    {
        err = driver->ops->control(device, cmd, arg);
    }
    return err;
}

NX_PRIVATE NX_Error UdriverMappable(NX_Driver *driver, struct NX_Device *device, NX_Size length, NX_U32 prot, NX_Addr * outPhyAddr)
{
    NX_Error err = NX_ENOFUNC;
    if (driver->ops->mappable)
    {
        err = driver->ops->mappable(device, length, prot, outPhyAddr);
    }
    return err;
}

NX_PRIVATE NX_Error UdriverPoll(NX_Driver *driver, struct NX_Device *device, NX_PollState * pState)
{
    NX_Error err = NX_ENOFUNC;
    if (driver->ops->poll)
    {
        err = driver->ops->poll(device, pState);
    }
    return err;
}

NX_PRIVATE NX_HubHandler hubDriverTable[] = 
{
    UdriverOpen,
    UdriverClose,
    UdriverRead,
    UdriverWrite,
    UdriverControl,
    UdriverMappable,
    UdriverPoll,
};

NX_Driver *NX_DriverCreate(const char *name, NX_DeviceType type, NX_U32 flags, NX_DriverOps *ops)
{
    if (name == NX_NULL || !ops)
    {
        return NX_NULL;
    }

    if (type < NX_DEVICE_TYPE_UNKNOWN || type >= NX_DEVICE_TYPE_NR)
    {
        return NX_NULL;
    }

    NX_Driver *driver = NX_MemAlloc(sizeof(NX_Driver));
    if (driver == NX_NULL)
    {
        return NX_NULL;
    }

    NX_StrCopy(driver->name, name);
    driver->type = type;
    driver->ops = ops;
    driver->flags = flags;
    driver->extension = NX_NULL;

    NX_ListInit(&driver->deviceListHead);
    return driver;
}

NX_Error NX_DriverDestroy(NX_Driver *driver)
{
    if (!driver)
    {
        return NX_EINVAL;
    }
    NX_MemFree(driver);
    return NX_EOK;
}

NX_PRIVATE NX_Device *__DeviceCreate(const char *name)
{
    if (name == NX_NULL)
    {
        return NX_NULL;
    }

    NX_Device *device = NX_MemAlloc(sizeof(NX_Device));
    if (device == NX_NULL)
    {
        return NX_NULL;
    }

    NX_ListInit(&device->list);
    device->driver = NX_NULL;
    device->extension = NX_NULL;
    NX_StrCopy(device->name, name);

    return device;
}

NX_PRIVATE NX_Error __DeviceDestroy(NX_Device *device)
{
    if (!device)
    {
        return NX_EINVAL;
    }
    NX_MemFree(device);
    return NX_EOK;
}

NX_PRIVATE NX_Error NX_DriverAttachDeviceObject(NX_Driver *driver, NX_Device *device)
{
    if (!driver || !device)
    {
        return NX_EINVAL;
    }

    NX_Device *tmp;
    NX_ListForEachEntry(tmp, &driver->deviceListHead, list)
    {
        if (!NX_StrCmp(tmp->name, device->name))
        {
            return NX_EAGAIN; /* meet same device */
        }
    }

    NX_ListAdd(&device->list, &driver->deviceListHead);
    device->driver = driver;
    return NX_EOK;
}

NX_PRIVATE NX_Error NX_DriverDetachDeviceObject(NX_Driver *driver, const char *name, NX_Device **device)
{
    if (!driver || !name || !device)
    {
        return NX_EINVAL;
    }

    NX_Device *tmp;
    NX_ListForEachEntry(tmp, &driver->deviceListHead, list)
    {
        if (!NX_StrCmp(tmp->name, name))
        {
            NX_ListDelInit(&tmp->list);
            *device = tmp;
            return NX_EOK;
        }
    }
    return NX_ENOSRCH;
}

NX_Error NX_DriverAttachDevice(NX_Driver *driver, const char *name, NX_Device **outDevice)
{
    if (!driver || !name)
    {
        return NX_EINVAL;
    }
    NX_Device *device = __DeviceCreate(name);
    if (device == NX_NULL)
    {
        return NX_ENOMEM;
    }
    if (outDevice)
    {
        *outDevice = device;
    }
    return NX_DriverAttachDeviceObject(driver, device);
}

NX_Error NX_DriverDetachDevice(NX_Driver *driver, const char *name)
{
    if (!driver || !name)
    {
        return NX_EINVAL;
    }
    
    NX_Device *device = NX_NULL;
    NX_Error err = NX_DriverDetachDeviceObject(driver, name, &device);
    if (err != NX_EOK)
    {
        return err;
    }
    NX_ASSERT(device);
    __DeviceDestroy(device);
    return NX_EOK;
}

NX_Error NX_DriverRun(NX_Solt driverSolt, NX_Driver * driver)
{
    NX_Error err;
    err = NX_HubRun(hubDriverTable, NX_ARRAY_SIZE(hubDriverTable), NX_HUB_WAIT);
    if (err != NX_EOK)
    {
        err = NX_DriverUnregister(driverSolt);
    }
    return err;
}
