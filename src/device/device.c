/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: device api
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-05-27     JasonHu           Init
 */

#include <nxos/syscall.h>
#include <nxos/device.h>

NX_Solt NX_DeviceOpen(const char * name, NX_U32 flags)
{
    NX_Solt solt = NX_SOLT_INVALID_VALUE;
    NX_Error err;

    NX_ErrorSet((err = NX_Syscall3(NX_API_DeviceOpen, name, flags, &solt)));
    return solt;
}

NX_Size NX_DeviceRead(NX_Solt solt, void *buf, NX_Offset off, NX_Size len)
{
    NX_Error err;
    NX_Size outLen = 0;

    NX_ErrorSet((err = NX_Syscall5(NX_API_DeviceRead, solt, buf, off, len, &outLen)));
    return outLen;
}

NX_Size NX_DeviceWrite(NX_Solt solt, void *buf, NX_Offset off, NX_Size len)
{
    NX_Error err;
    NX_Size outLen = 0;

    NX_ErrorSet((err = NX_Syscall5(NX_API_DeviceWrite, solt, buf, off, len, &outLen)));
    return outLen;
}

NX_Error NX_DeviceControl(NX_Solt solt, NX_U32 cmd, void *arg)
{
    NX_Error err;

    NX_ErrorSet((err = NX_Syscall3(NX_API_DeviceControl, solt, cmd, arg)));
    return err;
}

void * NX_DeviceMap(NX_Solt solt, NX_Size length, NX_U32 prot)
{
    NX_Error err;
    void * mapAddr = NX_NULL;

    NX_ErrorSet((err = NX_Syscall4(NX_API_DeviceMap, solt, length, prot, &mapAddr)));
    return mapAddr;
}

NX_Error NX_PollWait(NX_Solt * soltTable, NX_Size maxSoltCount, int timeout, NX_PollState * pState)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall4(NX_API_PollWait, soltTable, maxSoltCount, timeout, pState)));
    return err;
}

NX_Error NX_DeviceProbe(const char *name, NX_DeviceInfo *devinfo)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_DeviceProbe, name, devinfo)));
    return err;
}

NX_Error NX_DeviceEnum(NX_Offset offset, NX_DeviceInfo *devinfo)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_DeviceEnum, offset, devinfo)));
    return err;
}
