/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: virtual file system
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-4-1       JasonHu           Init
 */

#include <nxos/fs.h>
#include <nxos/syscall.h>

NX_WEAK_SYM NX_Error NX_FileSystemMount(const char * dev, const char * dir, const char * fsname, NX_U32 flags)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall4(NX_API_FileSystemMount, dev, dir, fsname, flags)));
    return err;
}

NX_WEAK_SYM NX_Error NX_FileSystemUnmount(const char * path)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall1(NX_API_FileSystemUnmount, path)));
    return err;
}

NX_WEAK_SYM NX_Error NX_FileSystemSync(void)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall(NX_API_FileSystemSync)));
    return err;
}

NX_WEAK_SYM NX_Solt NX_FileOpen(const char * path, NX_U32 flags, NX_U32 mode)
{
    NX_Solt solt = NX_SOLT_INVALID_VALUE;
    NX_Error err;

    err = NX_Syscall4(NX_API_FileOpen, path, flags, mode, &solt);
    NX_ErrorSet(err);
    
    return solt;
}

NX_WEAK_SYM NX_Error NX_FileClose(NX_Solt solt)
{
    return NX_SoltClose(solt);
}

NX_WEAK_SYM NX_Size NX_FileRead(NX_Solt solt, void * buf, NX_Size len)
{
    NX_Size ret = 0;
    NX_Error err;

    err = NX_Syscall4(NX_API_FileRead, solt, buf, len, &ret);
    NX_ErrorSet(err);
    return ret;
}

NX_WEAK_SYM NX_Size NX_FileWrite(NX_Solt solt, void * buf, NX_Size len)
{
    NX_Size ret = 0;
    NX_Error err;

    err = NX_Syscall4(NX_API_FileWrite, solt, buf, len, &ret);
    NX_ErrorSet(err);
    return ret;
}

NX_WEAK_SYM NX_Error NX_FileIoctl(NX_Solt solt, NX_U32 cmd, void *arg)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall3(NX_API_FileIoctl, solt, cmd, arg)));
    return err;
}

NX_WEAK_SYM NX_Offset NX_FileSetPointer(NX_Solt solt, NX_Offset off, int whence)
{
    NX_Offset ret = 0;
    NX_Error err;

    ret = NX_Syscall4(NX_API_FileSetPointer, solt, off, whence, &err);
    NX_ErrorSet(err);
    return ret;
}

NX_WEAK_SYM NX_Error NX_FileSync(NX_Solt solt)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall(NX_API_FileSync, solt)));
    return err;
}

NX_WEAK_SYM NX_Error NX_FileSetMode(NX_Solt solt, NX_U32 mode)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_FileSetMode, solt, mode)));
    return err;
}

NX_WEAK_SYM NX_Error NX_FileGetStat(NX_Solt solt, NX_FileStatInfo * st)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_FileGetStat, solt, st)));
    return err;
}

NX_WEAK_SYM NX_Solt NX_DirOpen(const char * name)
{
    NX_Solt solt = NX_SOLT_INVALID_VALUE;
    NX_Error err;
    
    err = NX_Syscall2(NX_API_DirOpen, name, &solt);
    NX_ErrorSet(err);

    return solt;
}

NX_WEAK_SYM NX_Error NX_DirClose(NX_Solt solt)
{
    return NX_SoltClose(solt);
}

NX_WEAK_SYM NX_Error NX_DirRead(NX_Solt solt, NX_Dirent * dir)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_DirRead, solt, dir)));
    return err;
}

NX_WEAK_SYM NX_Error NX_DirResetPointer(NX_Solt solt)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall1(NX_API_DirResetPointer, solt)));
    return err;
}

NX_WEAK_SYM NX_Error NX_DirCreate(const char * path, NX_U32 mode)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_DirCreate, path, mode)));
    return err;
}

NX_WEAK_SYM NX_Error NX_DirDelete(const char * path)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall1(NX_API_DirDelete, path)));
    return err;
}

NX_WEAK_SYM NX_Error NX_FileRename(const char * src, const char * dst)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_DirRename, src, dst)));
    return err;
}

NX_WEAK_SYM NX_Error NX_FileDelete(const char * path)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall1(NX_API_FileDelete, path)));
    return err;
}

NX_WEAK_SYM NX_Error NX_FileAccess(const char * path, NX_U32 mode)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_FileAccess, path, mode)));
    return err;
}

NX_WEAK_SYM NX_Error NX_FileSetModeToPath(const char * path, NX_U32 mode)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall1(NX_API_FileSetModeToPath, path)));
    return err;
}

NX_WEAK_SYM NX_Error NX_FileGetStatFromPath(const char * path, NX_FileStatInfo * st)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_FileGetStatFromPath, path, st)));
    return err;
}
