/**
 * Copyright (c) 2018-2024, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: message queue api
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2024-10-04     JasonHu           Init
 */

#include <nxos/syscall.h>
#include <nxos/msgqueue.h>

NX_Solt NX_MsgQueueCreate(NX_Size msgSize, NX_Size msgCount)
{
    NX_Solt solt = NX_SOLT_INVALID_VALUE;
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall3(NX_API_MsgQueueCreate, msgSize, msgCount, &solt)));
    return solt;
}

NX_Error NX_MsgQueueSend(NX_Solt msgQueue, NX_MsgBuf *buf)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_MsgQueueSend, msgQueue, buf)));
    return err;
}

NX_Error NX_MsgQueueTrySend(NX_Solt msgQueue, NX_MsgBuf *buf)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_MsgQueueTrySend, msgQueue, buf)));
    return err;
}

NX_Error NX_MsgQueueRecv(NX_Solt msgQueue, NX_MsgBuf *buf)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_MsgQueueRecv, msgQueue, buf)));
    return err;
}

NX_Error NX_MsgQueueTryRecv(NX_Solt msgQueue, NX_MsgBuf *buf)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_MsgQueueTryRecv, msgQueue, buf)));
    return err;
}

NX_Bool NX_MsgQueueFull(NX_Solt msgQueue)
{
    return NX_Syscall1(NX_API_MsgQueueFull, msgQueue);
}

NX_Bool NX_MsgQueueEmpty(NX_Solt msgQueue)
{
    return NX_Syscall1(NX_API_MsgQueueEmpty, msgQueue);
}

NX_Size NX_MsgQueueLenght(NX_Solt msgQueue)
{
    return NX_Syscall1(NX_API_MsgQueueLenght, msgQueue);
}
