/**
 * Copyright (c) 2018-2024, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: pipe
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2024-11-11     JasonHu           Init
 */

#include <nxos/syscall.h>
#include <nxos/ipc.h>
#include <nxos/utils.h>

NX_Solt NX_PipeCreate(const char *name, NX_Size size, NX_U8 flags)
{
    NX_Solt solt = NX_SOLT_INVALID_VALUE;
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall4(NX_API_PipeCreate, name, size, flags, &solt)));
    return solt;
}

NX_Solt NX_PipeConnect(const char *name)
{
    NX_Solt solt = NX_SOLT_INVALID_VALUE;
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_PipeConnect, name, &solt)));
    return solt;
}

NX_Size NX_PipeSend(NX_Solt solt, void *buffer, NX_Size len)
{
    NX_Error err;
    NX_Size nsend = 0;
    NX_ErrorSet((err = NX_Syscall4(NX_API_PipeSend, solt, buffer, len, &nsend)));
    return nsend;
}

NX_Size NX_PipeRecv(NX_Solt solt, void *buffer, NX_Size len)
{
    NX_Error err;
    NX_Size nrecv = 0;
    NX_ErrorSet((err = NX_Syscall4(NX_API_PipeRecv, solt, buffer, len, &nrecv)));
    return nrecv;
}

NX_Error NX_PipeDisconnect(NX_Solt solt)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall1(NX_API_PipeDisconnect, solt)));
    return err;
}
