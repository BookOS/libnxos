/**
 * Copyright (c) 2018-2022, NXOS Development Team
 * SPDX-License-Identifier: Apache-2.0
 * 
 * Contains: hub system
 * 
 * Change Logs:
 * Date           Author            Notes
 * 2022-4-7       JasonHu           Init
 */

#include <nxos/syscall.h>
#include <nxos/hub.h>

NX_Error NX_HubRegister(const char *name, NX_Size maxClient)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_HubRegister, name, maxClient)));
    return err;
}

NX_Error NX_HubUnregister(const char *name)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall1(NX_API_HubUnregister, name)));
    return err;
}

NX_Error NX_HubCallParamName(const char *name, NX_HubParam *param, NX_Size *retVal)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall3(NX_API_HubCallParamName, name, param, retVal)));
    return err;
}

NX_Error NX_HubReturn(NX_Size retVal, NX_Error retErr)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_HubReturn, retVal, retErr)));
    return err;
}

NX_Error NX_HubWait(NX_HubParam *param, NX_U32 flags)
{
    NX_Error err;
    NX_ErrorSet((err = NX_Syscall2(NX_API_HubWait, param, flags)));
    return err;
}

void *NX_HubTranslate(void *addr, NX_Size size)
{
    return (void *)NX_Syscall2(NX_API_HubTranslate, addr, size);
}

typedef NX_UArch (*NX_HubHandlerWithArg) (NX_UArch, NX_UArch, NX_UArch, NX_UArch, NX_UArch, NX_UArch, NX_UArch, NX_UArch);

NX_PRIVATE NX_HubHandler NX_HubGetHandler(NX_U32 api, NX_HubHandler handlerTable[], NX_Size tableSize)
{
    NX_HubHandler handler = NX_NULL;

    if (api < tableSize)
    {
        handler = handlerTable[api];
    }
    return handler;
}

NX_Error NX_HubDispatch(NX_HubParam *param, NX_HubHandler handlerTable[], NX_Size tableSize)
{
    NX_Error errVal = NX_EOK;
    NX_UArch retVal = 0;
    NX_HubHandlerWithArg handler;

    if (!param || !handlerTable || !tableSize)
    {
        return NX_EINVAL;
    }

    handler = (NX_HubHandlerWithArg)NX_HubGetHandler(param->api, handlerTable, tableSize);
    if (handler == NX_NULL)
    {
        errVal = NX_ENOFUNC;
    }
    else
    {
        retVal = handler(param->args[0], param->args[1], param->args[2], param->args[3],
                         param->args[4], param->args[5], param->args[6], param->args[7]);
    }

    return NX_HubReturn(retVal, errVal);
}

NX_Error NX_HubRun(NX_HubHandler handlerTable[], NX_Size tableSize, NX_U32 flags)
{
    NX_Error err;
    NX_HubParam param;
    while (1)
    {
        err = NX_HubWait(&param, flags);
        if (err == NX_ENORES)
        {
            return NX_ENORES;
        }
        else if (err == NX_EOK)
        {
            err = NX_HubDispatch(&param, handlerTable, tableSize);
            if (err != NX_EOK)
            {
                return err;
            }
        }
    }
}
