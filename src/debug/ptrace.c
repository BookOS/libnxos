#include <nxos/syscall.h>
#include <nxos/ptrace.h>

NX_Error NX_Ptrace(enum __ptrace_request request, NX_I32 tid, void *addr, void *data)
{
    return NX_Syscall(NX_API_Ptrace, request, tid, addr, data);
}