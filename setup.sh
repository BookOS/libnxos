#!/bin/bash

##
# Copyright (c) 2018-2022, NXOS Development Team
# SPDX-License-Identifier: Apache-2.0
# 
# Contains: shell scripts for nxos lib environment
# 
# Change Logs:
# Date           Author            Notes
# 2022-1-31      JasonHu           Init
##

# usage:
# source setup.sh [arch]
# example: source setup.sh           # x86
# example: source setup.sh riscv64   # riscv64
# set env
# set env
arch=$1
case $arch in 
    "x86")
        export CROSS_COMPILE=
        export ARCH=x86
        ;;
    "riscv64")
        export CROSS_COMPILE=riscv64-unknown-elf-
        export ARCH=riscv64
        ;;
    "qemu_riscv64")
        export CROSS_COMPILE=riscv64-unknown-elf-
        export ARCH=riscv64
        ;;
    *)  echo "unknown arch! " $arch
        return 1
esac

echo "Set environment for nxos-user."
echo "[CROSS COMPILE ] $CROSS_COMPILE"
echo "[ARCH          ] $arch"